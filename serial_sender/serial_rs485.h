#ifndef __SERIAL_RS485_H__
#define __SERIAL_RS485_H__

extern void setup_rs485(int rx_pin, int tx_pin, int enable_pin, int baud_rate);
extern int send_file(uint8_t *data, int file_size, uint8_t station);
extern bool check_rs485();
extern bool recv_rs485(byte *cmd, int *fileSize, uint8_t *pData);

#endif