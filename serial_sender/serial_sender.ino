#include <Arduino.h>
#include "serial_rs485.h"

#define RX_PIN 18
#define TX_PIN 19
#define EN_PIN -1

byte data[4096];

void setup() 
{
    Serial.begin(115200);
    delay(1000);
    Serial.println(" <Serial Send> \n");

    setup_rs485(RX_PIN, TX_PIN, EN_PIN, 115200*4);

    for (int i=0; i<4096; i++)
        data[i] = (i & 0xff);

    int size = send_file(data, 4096, 0);
    Serial.printf("sent size %d\n", size);
}


void loop() 
{
    delay(1000);
}

