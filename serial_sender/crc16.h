#ifndef __CRC16_H__
#define __CRC16_H__

extern uint16_t CRC16(uint8_t *buf, uint16_t len, uint16_t max_rBuf, uint16_t offset);
extern uint16_t Calc_Crc16(uint8_t *buf, uint16_t len, uint16_t max_rBuf, uint16_t offset);

#endif