#include <Arduino.h>
#include "serial_rs485.h"

#define RX_PIN 18
#define TX_PIN 19
#define EN_PIN -1

byte data[4096];
byte file_type = 0;
int file_size = 0;

void setup()
{
    Serial.begin(115200);
    delay(1000);
    Serial.println(" [Serial Recv] \n");

    setup_rs485(RX_PIN, TX_PIN, EN_PIN, 115200*4);
}


void loop() 
{
    unsigned long stime, etime;
    
    if (check_rs485())
    {
        Serial.println("recv start");
        stime = millis();
        if (recv_rs485(&file_type, &file_size, data))
        {
            etime = millis();
            Serial.printf("file type: 0x%02x, file size: %d, time %d \n", file_type, file_size, etime-stime);
            for (int i=0; i<file_size; i++)
            {
                if (0 == (i % 16)) Serial.println();
                Serial.printf("%02x ", data[i]);
            }
            Serial.println();
        }
        else
        {
            Serial.println("Fail recv_rs485");
        }
    }

    delay(10);
}