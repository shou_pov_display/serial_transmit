#include <Arduino.h>
#include <Update.h>
#include "crc16.h"

#define MAX_PACKET_DATA 240
#define MAX_TX_BUF 256
#define MAX_RX_BUF 256
#define SEND_HEADER 8
#define RECV_HEADER 6
#define CRC_SIZE 2
#define MAX_FRAME (SEND_HEADER + MAX_PACKET_DATA + CRC_SIZE)
#define MAX_RETRY   5
#define TIME_OUT 1000

byte rx_buf[MAX_RX_BUF];
byte tx_buf[MAX_RX_BUF];
HardwareSerial rs485(1);

byte my_station = 0xFF;
int rs485_enable_pin = -1;


void use_enable_pin(int pin)
{
    if (-1 < pin)
    {
        rs485_enable_pin = pin;  
        pinMode(rs485_enable_pin, OUTPUT);
    }
}


void enable_tx_rs485()
{
    if (-1 < rs485_enable_pin)
        digitalWrite(rs485_enable_pin, HIGH);
}


void disable_tx_rs485()
{
    if (-1 < rs485_enable_pin)
        digitalWrite(rs485_enable_pin, LOW);
}


void setup_rs485(int rx_pin, int tx_pin, int enable_pin, int baud_rate) 
{
    Serial.println("setup_rs485()");

    if (1 > rx_pin || 1 > tx_pin)
    {
        Serial.println("pin number must be larger than 0");
    }

    rs485.begin(baud_rate, SERIAL_8N1, rx_pin, tx_pin);
    rs485.setRxBufferSize(MAX_RX_BUF);
    use_enable_pin(enable_pin);
}


void set_station(uint8_t station)
{
    my_station = station;
}


void response_rs485(byte station, byte cmmd, uint16_t frame_no, byte err_code)
{
    int n = 0;

    enable_tx_rs485();
    tx_buf[n++] = 0x42;
    tx_buf[n++] = station;
    tx_buf[n++] = cmmd;
    tx_buf[n++] = frame_no >> 8; // v2
    tx_buf[n++] = frame_no;
    tx_buf[n++] = err_code;
    rs485.write(tx_buf, n);
    rs485.flush();
    disable_tx_rs485();
}


bool check_rs485()
{
    bool result = false;

    int num = rs485.available();

    if (num)
    {
        if (SEND_HEADER <= num)
        {
            result = true;
        }
        else
        {
            do
            {
                byte c = rs485.read();
                Serial.printf("<%02X>", c);
            } while (rs485.available() > 0);
        }
    }
    return result;
}


bool recv_rs485(byte *cmd, int *fileSize, uint8_t data[])
{
    int start = 0;
    int index = 0;
    int f_index = 0;
    int recv_time = 0;
    uint16_t save_frame_no = 0xffff;
    uint16_t temp_frame_no = 0;
    uint16_t frame_no = 0;
    uint16_t data_len = 0;
    uint16_t temp = 0;
    uint16_t recv_crc = 0;
    uint16_t calc_crc = 0;
    int length = 0;
    int  total_size = 0;
    uint16_t offset = 0;
    byte info = 0;
    byte cmmd = 0;
    byte err_code = 0;
    byte station = 0;
    bool overlap = false;
    bool give_up = false;
    bool result = false;
    bool starter = false;
    
    memset(rx_buf, 0, MAX_RX_BUF);

    do
    {
        index = 0;
        length = 0;
        err_code = 0;

        //if (rs485_enable_pin > -1) // RS485
        {
            if (rs485.available())
            {
                do
                {
                    rs485.readBytes(&rx_buf[0], 1);
                    if (0x81 == rx_buf[0])
                    {
                        length = 1;
                        starter = true;
                        break;
                    }
                    else
                        Serial.printf("[%02x],", rx_buf[0]);
                } while (rs485.available() && false == starter);
            }
        }

        if (starter)
        {
            starter = false;
            length += rs485.readBytes(&rx_buf[1], SEND_HEADER-1);
        }
        else
        {
            length += rs485.readBytes(&rx_buf[index], SEND_HEADER);
        }

        if (0 < length)
        {
            index += length;
            recv_time = millis();
            // Serial.printf("RECV%d ", length);
        }

        if (SEND_HEADER <= length)
        {
            if (0x81 != rx_buf[0] || my_station != rx_buf[1])
            {
                err_code = 0x01;
                Serial.printf("error frame index %d \n", length);
                for (int i=0; i<length; i++)
                   Serial.printf("%02x ", rx_buf[i]);
                Serial.println();
                
            }
            else
            {
                cmmd = rx_buf[2];
                frame_no = (rx_buf[3] << 8) | rx_buf[4];
                temp_frame_no = save_frame_no + 1;
                if (frame_no == temp_frame_no)
                {
                    overlap = false; // normal
                }
                else if (frame_no == save_frame_no)
                {
                    overlap = true; // duplicated frame
                }
                else if (frame_no != temp_frame_no)
                {
                    err_code = 0x02;
                    Serial.printf("error frame no %d %d \n", temp_frame_no, frame_no);
                    overlap = false;
                }
                data_len = (rx_buf[5] << 8) | rx_buf[6];
                info = rx_buf[7];
                if (0xF0 > info || info > 0xF2)
                {
                    err_code = 0x04;
                    Serial.printf("error info %02x \n", info);
                }
                if (0xF0 == info)
                    offset = 2;
                else
                    offset = 0;

                // Serial.printf("cmd:%d frameNo:%-8d dataLen:%d info:%02X \n", cmmd, frame_no, data_len, info);

                if (0x00 == err_code)
                {
                    length = rs485.readBytes(&rx_buf[index], data_len+2);
                    if (0 < length)
                    {
                        index += length;
                        recv_time = millis();
                    }

                    byte crc_l = rx_buf[SEND_HEADER + data_len];
                    byte crc_h = rx_buf[SEND_HEADER + data_len + 1];
                    uint16_t recv_crc = crc_h << 8 | crc_l;
                    uint16_t calc_crc = CRC16(rx_buf, SEND_HEADER+data_len, MAX_RX_BUF, 0);
                    //uint16_t calc_crc = Calc_Crc16(rx_buf, SEND_HEADER+data_len, MAX_RX_BUF, 0);
                    if (recv_crc != calc_crc)
                    {
                        err_code = 0x05;
                        Serial.printf("crc_l %02x %02x\n", crc_l, crc_h);
                        Serial.printf("error crc %04x %04x\n", recv_crc, calc_crc);
                    }
                }

                if (true == overlap)
                {
                    Serial.println("frame overlaped.");
                }
                else if (0x00 == err_code)
                {
                    memcpy(&data[f_index], &rx_buf[SEND_HEADER+offset], data_len-offset);
                    f_index += (data_len - offset);
                    save_frame_no = frame_no;
                    if (0xF0 == info)
                        total_size = (rx_buf[8] << 8) | rx_buf[9];
                    response_rs485(my_station, cmmd, save_frame_no, err_code);

                    // Serial.printf("\n rx_buf \n");
                    // for (int i=0; i<250+offset; i++)
                    //     Serial.printf("%02x", rx_buf[i]);
                    // Serial.printf("\n\n");
                    if (err_code)
                        Serial.printf("err code %d\n", err_code);
                    // Serial.printf("data OK %d / %d \n", f_index, total_size);  

                    memset(rx_buf, 0, MAX_RX_BUF);               
                    if (0xF2 == info)
                        break;
                }
                else
                {
                    /* make empty buffer */
                    do
                    {
                        length = rs485.available();
                        if (length)
                            rs485.read();
                    }
                    while (length);
                }
            }
        }
        else
        {
            delay(10);
        }

        do
        {
            if (millis() - recv_time > TIME_OUT)
            {
                Serial.printf("TIME OUT %d sec \n", TIME_OUT);
                give_up = true;
                break;
            }
            length = rs485.available();
        } while (0 == length);

        if (give_up)
            break;
    }
    while (0xF2 != info);

    if (0xF2 == info && 0 == err_code && f_index == total_size)
    {
        *cmd = cmmd;
        *fileSize = f_index;
        // Serial.printf("result %d / %d \n", f_index, total_size);
        result = true;
    }

    return result;
}


int send_file(uint8_t *data, int file_size, uint8_t station)
{
    int      sent_size = 0;
    uint16_t frame_no = 0;
    uint16_t data_len = 0;
    uint16_t length = 0;
    uint16_t crc = 0;
    uint16_t idx = 0;
    uint16_t retry = 0;
    unsigned long send_time = 0;
    unsigned long curr_time = 0;
    unsigned long time_out = 0;
    int total_size = 0;
    uint16_t offset = 0;    
    bool     stop_trans = false;
    uint8_t  info = 0xF0;
    uint8_t  cmd = 1;

    total_size = file_size;
    while (file_size)
    {
        idx = 0;
        if (file_size >= MAX_PACKET_DATA)
        {
            data_len = MAX_PACKET_DATA;
            file_size = file_size - MAX_PACKET_DATA;            
        }
        else
        {
            data_len = file_size;
            file_size = 0;
        }

        if (0 == frame_no)
        {
            info = 0xF0;  // start frame
            offset = 2;
        }
        else
        {
            if (0 == file_size)
                info = 0xF2;  // end frame
            else
                info = 0xF1;  // middle frame
            offset = 0;
        }

        /* Header */
        tx_buf[idx++] = 0x81;
        tx_buf[idx++] = my_station;
        tx_buf[idx++] = cmd;
        tx_buf[idx++] = (frame_no >> 8) & 0xFF;
        tx_buf[idx++] = frame_no & 0xFF;
        tx_buf[idx++] = ((data_len + offset) >> 8) & 0xFF;
        tx_buf[idx++] = (data_len + offset) & 0xFF;
        tx_buf[idx++] = info;
        /* Data */        
        if (0xf0 == info)
        {
            tx_buf[idx++] = (total_size >> 8) & 0xFF;
            tx_buf[idx++] = total_size & 0xFF;
        }
        memcpy(&tx_buf[SEND_HEADER+offset], &data[sent_size], data_len);
        crc = CRC16(tx_buf, SEND_HEADER+data_len+offset, MAX_RX_BUF, 0);
        idx += data_len;
        tx_buf[idx++] = crc & 0xff;        // low
        tx_buf[idx++] = (crc >> 8) & 0xFF; // high

        enable_tx_rs485();
        rs485.write(tx_buf, idx);
        // Serial.printf("send %d\n", idx);
        // for (int i=0; i<idx; i++)
        //     Serial.printf("%02x ", tx_buf[i]);
        // Serial.println();
        rs485.flush();
        disable_tx_rs485();
        send_time = millis();

        memset(rx_buf, 0, 256);
        while (true)
        {
            length = rs485.available();
            if (RECV_HEADER <= length)
            {
                length = rs485.readBytes(&rx_buf[0], length);
                // for (int i=0; i<length; i++)
                //    Serial.printf("%02x ", rx_buf[i]);
                // Serial.println();
                if (0x42 == rx_buf[0] && station == rx_buf[1] && cmd == rx_buf[2])
                    if (tx_buf[3] == rx_buf[3] && tx_buf[4] == rx_buf[4] && 0x00 == rx_buf[5])
                    {
                        sent_size += data_len;
                        break;
                    }
                    else
                        Serial.printf("frame error \n");
                else
                    Serial.printf("recv error \n");
            }
            else
            {
                delay(10);
            }

            if (0 == frame_no)
                time_out = 5000;
            else
                time_out = TIME_OUT;
            curr_time = millis();
            if (time_out < (curr_time - send_time))
            {
                retry += 1;
                Serial.println("TIME OUT");
                if (retry < MAX_RETRY)
                {
                    Serial.printf("retry %d\n", retry);
                    rs485.write(tx_buf, idx);
                    send_time = millis();
                }
                else
                {
                    Serial.println("Error, Stop transfer, Bye");
                    stop_trans = true;
                    break;
                }
            }
        }

        if (stop_trans)
            break;

        frame_no += 1;

        if (0 == file_size)
            break;
    } // while
    
    Serial.println("end transfer");
    return sent_size;
}
